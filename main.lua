

local alarm = grp.getvalue("3/1/1")
local target = grp.tag("LUMI")
local target_ni = grp.tag("LUMI-NI")
local night = grp.getvalue("3/1/9")

function light_on()
    for i, obj in ipairs(target) do
        obj:write(0)
        sleep(1)
    end
    for i, obj in ipairs(target_ni) do
        obj:write(1)
        sleep(1)
    end
end

function light_off()
    for i, obj in ipairs(target) do
        obj:write(1)
        sleep(1)
    end
    for i, obj in ipairs(target_ni) do
        obj:write(0)
        sleep(1)
    end
end



function main()
    log("script triggered !!!")
    if not alarm and night then
        light_on()
    elseif not alarm and not night then
        light_off()
    elseif alarm then
        light_off()
    end
end

main()
